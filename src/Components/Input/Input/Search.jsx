import React from 'react';
import { ReactComponent as Icon } from '../../../Assets/search.svg';

const defaultProps = {
    type: "text",
    value: "",
    onChange: null,
    options: []
};

const Input = props => {
    props = { ...defaultProps, ...props };
    return (
        <div className="input--search">
            <input
                placeholder={props.placeholder}
                className="input__input"
                type="text"
                value={props.value}
                onChange={(el)=>props.onChange(el.target.value)}
            />
            <Icon className="input__icon"></Icon>
        </div>

    );
};

export default Input;
