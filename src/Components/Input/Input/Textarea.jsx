import React from 'react';

const defaultProps = {
    type: "text",
    value: "",
    onChange: null,
    options: []
};

const Input = props => {
    props = { ...defaultProps, ...props };
    return (
        <textarea
            className="input--textarea"
            value={props.value}
            onChange={(el)=>props.onChange(el.target.value)}
        >
        </textarea>
    );
};

export default Input;
