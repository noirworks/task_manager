import React from 'react';

const defaultProps = {
    type: "text",
    value: "",
    onChange: null,
    options: []
};

const Input = props => {
    props = { ...defaultProps, ...props };
    return (
        <input
            className="input--text"
            type="text"
            value={props.value}
            onChange={(el)=>props.onChange(el.target.value)}
        />
    );
};

export default Input;
