import React from 'react';
import * as actionSearchform from '../../Store/Action/formSearch';
import {connect} from 'react-redux';
import Input from '../Input/Input';
import txt from '../../Content/lang';

const Searchbar = props => {
    return (
        <div className="Searchbar">
            <Input
                placeholder={txt.searchPlaceholder}
                type="search"
                onChange={(value)=>props.onChange({ phrase: value })}
                value={props.phrase}
            />
        </div>
    );
};

const mapStateToProps = (state) => {
    return state.searchForm;
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChange: (data) => dispatch(actionSearchform.onChange(data))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Searchbar);
