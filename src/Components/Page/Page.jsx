import React from 'react';
import './Page.scss';

const Page = (props) => (
    <div className="page">
        <div className="page__content">
            {props.children}
        </div>
    </div>
);

export default Page;
