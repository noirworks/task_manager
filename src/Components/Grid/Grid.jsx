import React from 'react';
import './Grid.scss';

const defaultProps = {
    className: '',
    mod: '',
    children: null
};

const getClassName = (props) => {
    let className = 'grid';
    if(props.mod){ className += ' grid--' + props.mod; };
    if(props.className){ className += ' ' + props.className; };
    return className;
};

const Grid = props => {
    props = { ...defaultProps, ...props };
    const className = getClassName(props);
    return (
        <div className={className}>
            { props.children }
        </div>
    );
};

export default Grid;
