import React from 'react';
import txt from '../../Content/lang';
import './Task.scss';

const Task = props => {
    const task = props.task;
    let deadline = null;
    if(props.task.deadline && props.task.deadline.date){
        let date = new Date(props.task.deadline.date);
        deadline = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }
    return (
        <div className="task">
            <div className="task__row">
                <span className="task__label">
                    {txt.subject}:
                </span>
                <span className="task__value">
                    {task.subject}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.category}:
                </span>
                <span className="task__value">
                    {task.category}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.description}:
                </span>
                <span className="task__value">
                    {task.description}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.priority}:
                </span>
                <span className="task__value">
                    {task.priority}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.deadline}:
                </span>
                <span className="task__value">
                    {deadline}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.firstname}:
                </span>
                <span className="task__value">
                    {task.userFirstname}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.surname}:
                </span>
                <span className="task__value">
                    {task.userSurname}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.mail}:
                </span>
                <span className="task__value">
                    {task.userMail}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.comments}:
                </span>
                <span className="task__value">
                    {task.comments}
                </span>
            </div>
        </div>
    );
};

export default Task ;
