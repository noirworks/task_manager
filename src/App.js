import React from 'react';
import './App.css';
import * as actionSearch from './Store/Action/search';
import {connect} from 'react-redux';
import { BrowserRouter as Router, Route } from "react-router-dom";
import PageSingle from './Page/Single';
import PageResults from './Page/Results';
import PageFormular from './Page/Formular';
import Page from './Components/Page/Page';
import Btn from './Components/Btn/Btn';
import Section from './Components/Section/Section';
import Grid from './Components/Grid/Grid';
import txt from './Content/lang';

class App extends React.Component{

    componentDidMount = () => {
        this.getResults();
    };

    getResults = () => {
        this.props.getTasks();
    };

    render = () => {
        return (
            <div className="App">
                 <Router basename={''}>
                    <Page>
                        <Section mod="nav">
                            <Grid mod ="nav" >
                                <Btn
                                    tag="Link"
                                    to="/"
                                    size="lg"
                                >
                                    {txt.homepageLink}
                                </Btn>
                                <Btn
                                    tag="Link"
                                    to="/new-task"
                                    size="lg"
                                >
                                    {txt.newTaskLink}
                                </Btn>
                            </Grid>
                        </Section>
                        <Section mod="content">
                            <Route path="/" exact component={ PageResults } />
                            <Route path="/task/:id" exact component={ PageSingle } />
                            <Route path="/new-task" exact component={ PageFormular } />
                        </Section>
                    </Page>
                </Router>
            </div>
        );
    };

}

const mapStateToProps = (state) => {
    return state.tasks;
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTasks: (data) => dispatch(actionSearch.getTasks(data))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(App);

