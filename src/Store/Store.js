import { createStore, applyMiddleware } from 'redux';
import reducer from './Reducer';
import thunk from 'redux-thunk';
import initialState from './InitialState';

const store = createStore(
    reducer,
    initialState,
    applyMiddleware(thunk)
);

export default store;
