export default {
    searchForm: {
        phrase: '',
    },
    tasks: {
        pending: false,
        error: false,
        items: [],
    },
    createForm: {
        pending: false,
        description: '',
        priority: '',
        deadline: '',
        category: '',
        subject: '',
        userFirstname: '',
        userSurname: '',
        userMail: '',
        comments: ''
    }
};
