//import axios from 'axios';

export const DISABLE_TASK_REQUESTED = 'DISABLE_TASK_REQUESTED';
export const DISABLE_TASK_DONE = 'DISABLE_TASK_DONE';
export const DISABLE_TASK_FAILED = 'DISABLE_TASK_FAILED';

export function disableTaskRequested() {
    return {
        type: 'DISABLE_TASK_REQUESTED'
    };
}

export function disableTaskDone(id) {
    return {
        type: 'DISABLE_TASK_DONE',
        payload: id
    };
}

export function disableTaskFailed(error) {
    return {
        type: 'DISABLE_TASK_FAILED',
        payload: error
    };
}

export function disableTask( data ) {
    return dispatch => {
        dispatch(disableTaskRequested());
        /*let fd = new FormData();
        fd.append('action','shippings');
        axios.post(process.env.API_URL, fd)
            .then( (response) => {
                if( response.data.success ){
                    dispatch(getDataDone(response.data));
                }
            })
            .catch( (error) => {
                dispatch(getDataFailed(error));
            })
        ;*/
    }
}

