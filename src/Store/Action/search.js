import axios from '../Axios';
import {build} from 'search-params';

export const GET_TASKS_REQUESTED = 'GET_TASKS_REQUESTED';
export const GET_TASKS_DONE = 'GET_TASKS_DONE';
export const GET_TASKS_FAILED = 'GET_TASKS_FAILED';

export function getTasksRequested() {
    return {
        type: 'GET_TASKS_REQUESTED'
    };
}

export function getTasksDone(result) {
    return {
        type: 'GET_TASKS_DONE',
        payload: result
    };
}

export function getTasksFailed(error) {
    return {
        type: 'GET_TASKS_FAILED',
        payload: error
    };
}

export function getTasks( data = null ) {
    return dispatch => {

        let search = "";
        if(data){
            search = build(data);
        }

        let url = '/tasks';
        if(search){ url += '?' + search; };

        dispatch(getTasksRequested());
        axios({
                'method': 'get',
                'url': url,
            })
            .then((response) => {
               dispatch(getTasksDone(response.data));
            })
            .catch( (error) => {
                dispatch(getTasksFailed(error));
            })
        ;
    }
}

