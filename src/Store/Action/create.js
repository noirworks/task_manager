import axios from '../Axios';

export const CREATE_TASK_REQUESTED = 'CREATE_TASK_REQUESTED';
export const CREATE_TASK_DONE = 'CREATE_TASK_DONE';
export const CREATE_TASK_FAILED = 'CREATE_TASK_FAILED';

export function createTaskRequested() {
    return {
        type: 'CREATE_TASK_REQUESTED'
    };
}

export function createTaskDone(id) {
    return {
        type: 'CREATE_TASK_DONE',
        payload: id
    };
}

export function createTaskFailed(error) {
    return {
        type: 'GET_DATA_FAILED',
        payload: error
    };
}

export function createTask(data) {
    return dispatch => {
        dispatch(createTaskRequested());
        const url = '/task';
        axios({
            'method': 'post',
            'url': url,
            data: data
            })
            .then((response) => {
                dispatch(createTaskDone(response.data));
            })
            .catch( (error) => {
                dispatch(createTaskFailed(error));
            })
        ;
    }
}

