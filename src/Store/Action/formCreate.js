export const FORM_CREATE_CHANGE = 'FORM_CREATE_CHANGE';

export function formCreateChangeHandler(fields) {
    return {
        type: 'FORM_CREATE_CHANGE',
        payload: fields
    };
}

export function changeHandler( fields ) {
    return dispatch => {
        dispatch(formCreateChangeHandler(fields));
    }
}
