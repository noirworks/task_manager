//import axios from 'axios';

export const EDIT_TASK_REQUESTED = 'EDIT_TASK_REQUESTED';
export const EDIT_TASK_DONE = 'EDIT_TASK_DONE';
export const EDIT_TASK_FAILED = 'EDIT_TASK_FAILED';

export function editTaskRequested() {
    return {
        type: 'EDIT_TASK_REQUESTED'
    };
}

export function editTaskDone(id) {
    return {
        type: 'EDIT_TASK_DONE',
        payload: id
    };
}

export function editTaskFailed(error) {
    return {
        type: 'EDIT_TASK_FAILED',
        payload: error
    };
}

export function editTask( data ) {
    return dispatch => {
        dispatch(editTaskRequested());
        /*let fd = new FormData();
        fd.append('action','shippings');
        axios.post(process.env.API_URL, fd)
            .then( (response) => {
                if( response.data.success ){
                    dispatch(getDataDone(response.data));
                }
            })
            .catch( (error) => {
                dispatch(getDataFailed(error));
            })
        ;*/
    }
}

