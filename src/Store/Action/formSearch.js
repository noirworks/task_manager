export const FORM_SEARCH_CHANGE = 'FORM_SEARCH_CHANGE';

export function formSearchChangeHandler(fields) {
    return {
        type: 'FORM_SEARCH_CHANGE',
        payload: fields
    };
}

export function onChange( fields ) {
    return dispatch => {
        dispatch(formSearchChangeHandler(fields));
    }
}
