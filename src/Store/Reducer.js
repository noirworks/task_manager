import * as searchActions from './Action/search';
import * as createActions from './Action/create';
//import * as editActions from './Action/edit';
//import * as disableActions from './Action/disable';
import * as formCreateActions from './Action/formCreate';
import * as formSearchActions from './Action/formSearch';

import initialState from './InitialState';

const reducer = (state = initialState, action) => {

    switch (action.type) {

        //get tasks result
        case searchActions.GET_TASKS_REQUESTED:
            return {...state, tasks: { pending: true, error: false, items: [] }};

        case searchActions.GET_TASKS_DONE:
            return {...state, tasks: { pending: false, error: false, items: action.payload.tasks }};

        case searchActions.GET_TASKS_FAILED:
            return {...state, tasks: { pending: false, error: action.payload, items: [] }};


        //create

        case createActions.CREATE_TASK_REQUESTED:
            return {...state, createForm: { ...state.createForm, pending: true} };

        case createActions.CREATE_TASK_DONE:
            return {
                ...state,
                createForm: {
                    ...initialState.createForm,
                    pending: false,
                },
                tasks: {
                    pending: false,
                    error: false,
                    items: [...state.tasks.items, action.payload.task]
                }
            };

        case createActions.CREATE_TASK_FAILED:
            return {
                ...state,
                createForm: {
                    ...initialState.createForm,
                    pending: false,
                },
                tasks: {
                    pending: false,
                    error: action.payload.tasks,
                    items: state.tasks.items
                }
            };


/*
        //edit
        case searchActions.GET_TASKS_REQUESTED:
            return {...state, tasks: { pending: true, error: false, items: [] }};

        case searchActions.GET_TASKS_DONE:
            return {...state, tasks: { pending: false, error: false, items: action.payload.tasks }};

        case searchActions.GET_TASKS_FAILED:
            return {...state, tasks: { pending: false, error: action.payload, items: [] }};

        //stop
        case disableActions.TASK_STOP_REQUESTED:
            return {...state, tasks: { pending: true, error: false, items: [] }};

        case disableActions.TASK_STOP_DONE:
            return {...state, tasks: { pending: false, error: false, items: action.payload.tasks }};

        case disableActions.TASK_STOP_FAILED:
            return {...state, tasks: { pending: false, error: action.payload, items: [] }};

        //start
        case searchActions.TASK_START_REQUESTED:
            return {...state, tasks: { pending: true, error: false, items: [] }};

        case searchActions.TASK_START_DONE:
            return {...state, tasks: { pending: false, error: false, items: action.payload.tasks }};

        case searchActions.TASK_START_FAILED:
            return {...state, tasks: { pending: false, error: action.payload, items: [] }};
*/
        //CreateForm
        case formCreateActions.FORM_CREATE_CHANGE:
            return {...state, createForm: { ...state.createForm, ...action.payload }};

        //Searchform
        case formSearchActions.FORM_SEARCH_CHANGE:
            return {...state, searchForm: { ...state.searchForm, ...action.payload }};

        default:
            return state;
    }

};

export default reducer;
