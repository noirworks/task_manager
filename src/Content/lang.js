export default {
    resultsError: "Wystąpił błąd w komunikacji z serwerem.",
    resultsEmpty: "Nie znaleziono żadnych elementów.",
    resultsPending: "Trwa pobieranie wyników wyszukiwania...",
    subject:'temat',
    category:'kategoria',
    description:'opis',
    priority:'priorytet',
    deadline:'deadline',
    firstname:'imię',
    surname:'nazwisko',
    mail:'mail',
    comments:'komentarz',
    homepageLink: 'Lista tasków',
    newTaskLink: 'Stwórz nowy task',
    searchPlaceholder: 'Wyszukaj zadań o podanym temacie..',
    createTaskSubmit: 'Zarejestruj zadanie'
};
