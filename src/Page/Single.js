import React from 'react';
import {connect} from 'react-redux';
import Task from '../Components/Task/Task';

class Page extends React.Component{

    getTask = () => {
        let find = this.props.items.find((item)=>{
            if( parseInt(item.id) === parseInt(this.props.match.params.id) ){
                return item;
            };return false;
        });
        return find;
    };

    render = () => {

        let task = this.getTask();
        let card = null;
        if(task){
            card = (
                <Task
                    task={task}
                />
            );
        };
        return (
            <div>
                { card }
            </div>
        );
    };
}

const mapStateToProps = (state) => {
    return state.tasks;
};

const mapDispatchToProps = (dispatch) => {
    return {
    //    getTasks: (data) => dispatch(actionSearch.getTasks(data))
    //edit
    //start
    //stop
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Page);
