import React from 'react';
import * as actionSearch from '../Store/Action/search';
import {connect} from 'react-redux';
import Searchbar from '../Components/Searchbar/SearchBar';
import txt from '../Content/lang';
import Section from '../Components/Section/Section';
import Task from '../Components/Task/Card';
import Grid from '../Components/Grid/Grid';

class Page extends React.Component{

    searchbarSubmitHandler = (phrase) => {
        this.props.getTasks({
            'phrase': phrase
        });
    };

    render = () => {

        let content = null;
        if(this.props.error){
            content = txt.resultsError;
        }else if(this.props.pending){
            content = txt.resultsPending;
        }else if(this.props.items.length === 0){
             content = txt.resultsEmpty;
        }else if(this.props.items.length > 0){
            let items = [];
            if(this.props.items && this.props.items.length){
                if(this.props.phrase){
                    items = this.props.items.filter((task)=>{
                        return (task.subject && task.subject.length && task.subject.includes(this.props.phrase));
                    });
                }else{
                    items = this.props.items;
                }
            }
            let tasks = items.map((task,key)=>{
                return (
                    <Task key={key} task={task}></Task>
                );
            });
            content = (
                <Grid mod="tasks">
                    {tasks}
                </Grid>
            );
        }

        return (
            <div className="content">
                <Section mod="nav">
                    <Searchbar
                        onSubmit={this.searchbarSubmitHandler}
                        pending={this.props.pending}
                    />
                    </Section>
                <Section>
                    {content}
                </Section>
            </div>
        );
    };

}

const mapStateToProps = (state) => {
    return { ...state.tasks, phrase: state.searchForm.phrase };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTasks: (data) => dispatch(actionSearch.getTasks(data))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Page);
