import React from 'react';
import * as formHandler from '../Store/Action/formCreate';
import * as actionCreate from '../Store/Action/create';
import {connect} from 'react-redux';
import txt from '../Content/lang';
import Input from '../Components/Input/Input';
import Btn from '../Components/Btn/Btn';
import Section from '../Components/Section/Section';

class Page extends React.Component{

    render = () => {
        return (
            <div className="task">
                <Section>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.subject}:
                        </span>
                        <Input
                            type="text"
                            value={this.props.subject}
                            onChange={(value)=>this.props.onChange({subject: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.category}:
                        </span>
                        <Input
                            type="text"
                            value={this.props.category}
                            onChange={(value)=>this.props.onChange({category: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.description}:
                        </span>
                        <Input
                            type="textarea"
                            value={this.props.description}
                            onChange={(value)=>this.props.onChange({description: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.priority}:
                        </span>
                        <Input
                            type="select"
                            options={[{value:'a'},{value:'b'},{value:'c'}]}
                            value={this.props.priority}
                            onChange={(value)=>this.props.onChange({priority: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.deadline}:
                        </span>
                        <Input
                            type="datetime"
                            value={this.props.deadline}
                            onChange={(value)=>this.props.onChange({deadline: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.firstname}:
                        </span>
                        <Input
                            type="text"
                            value={this.props.userFirstname}
                            onChange={(value)=>this.props.onChange({userFirstname: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.surname}:
                        </span>
                        <Input
                            type="text"
                            value={this.props.userSurname}
                            onChange={(value)=>this.props.onChange({userSurname: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.mail}:
                        </span>
                        <Input
                            type="text"
                            value={this.props.userMail}
                            onChange={(value)=>this.props.onChange({userMail: value})}
                        />
                    </div>
                    <div className="task__row">
                        <span className="task__label">
                            {txt.comments}:
                        </span>
                        <Input
                            type="textarea"
                            value={this.props.comments}
                            onChange={(value)=>this.props.onChange({comments: value})}
                        />
                    </div>
                </Section>
                <Section>
                    <div className="task__row">
                        <Btn
                            mod="primary"
                            size="md"
                            onClick={()=>this.props.onSubmit({
                                subject: this.props.subject,
                                category: this.props.category,
                                description: this.props.description,
                                priority: this.props.priority,
                                deadline: this.props.deadline,
                                userFirstname: this.props.userFirstname,
                                userSurname: this.props.userSurname,
                                userMail: this.props.userMail,
                                comments: this.props.comments
                            })}
                        >
                            {txt.createTaskSubmit}
                        </Btn>
                    </div>
                </Section>
            </div>
        );
    };
}

const mapStateToProps = (state) => {
    return state.createForm;
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChange: (data) => dispatch(formHandler.changeHandler(data)),
        onSubmit: (data) => dispatch(actionCreate.createTask(data)),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Page);
